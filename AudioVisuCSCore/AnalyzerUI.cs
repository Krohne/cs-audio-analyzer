﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CSCore;
using CSCore.Streams;
using CSCore.SoundIn;
using CSCore.DSP;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace AudioVisuCSCore
{
    public partial class AnalyzerUI : Form
    {
        //Audio Stuff
        private readonly WasapiCapture _soundIn;
        private readonly IWaveSource _finalSource;
        //L,R,fft Audio buffer
        //private readonly List<float> _left;
        //private readonly List<float> _right;
        private readonly int _waveFormSize;
        //block to store one fft-size of samples
        private float[] _block;
        //samples used to draw the waveform in the oscilloscope
        private float[] _samples;
        //fft-size
        private readonly static int _fftSize = 1024;
        //buffer for fft-results
        private readonly float[] fftBuffer = new float[_fftSize];
        //fft-results of the last timestep, stored to implement smoothing eventually 
        //(technically it should already be implemented but it doesnt work :^))
        private float[] oldfftBuffer = new float[_fftSize];
        //smoothing factor: 1 means just reapply old results to graph, 0 means only take the most recent results
        private float _smooth = 1f;
        //Global variable for spectrum size
        private readonly static int _spectrumSize = _fftSize / 2;
        //Mapping for bins to frequencies
        private readonly float[] _freqMap = new float[_spectrumSize];
        //Spectrum decibel container
        private float[] _decibelSpectrum = new float[_spectrumSize];
        //Points for Drawing Spectrum
        private PointF[] _spectrumCurve = new PointF[_spectrumSize];
        //Object for locking
        private readonly object _lockObj = new object();

        //Raw Audio Buffer
        readonly byte[] buffer;
        //Global variable for bytes per second
        int _bytesPerSecond = -1;
        //Global variable for buffersize of "drawing buffers"
        int _actualBufferSize = -1;
        

        //FFT Provider for Spectrum display
        readonly FftProvider fftProvider = new FftProvider(2, (FftSize)_fftSize);

        public AnalyzerUI()
        {
            InitializeComponent();

            //add this to make sure the audio action only happens when the UI and stuff is done loading
            this.Shown += new System.EventHandler(this.AnalyzerUIShown);
            //Init Picturebox Images
            Image waveFormImage = new Bitmap(waveFormBox.Width, waveFormBox.Height);
            Graphics waveFormGraphics = Graphics.FromImage(waveFormImage);
            Image spectrumImage = new Bitmap(spectrumDisplay.Width, spectrumDisplay.Height);
            Graphics spectrumGraphics = Graphics.FromImage(spectrumImage);
            waveFormBox.Image = waveFormImage;
            spectrumDisplay.Image = spectrumImage;

            //Init Audio Stuff
            _soundIn = new WasapiLoopbackCapture();
            _soundIn.Initialize();
            var soundInSource = new SoundInSource(_soundIn);
            //var singleBlockNotificationStream = new SingleBlockNotificationStream(soundInSource.ToSampleSource());
            var NotificationSource = new NotificationSource(soundInSource.ToSampleSource());
            //A "block" is one sample for each channel, but we only want to read once we collected _fftSize new samples
            NotificationSource.BlockCount = _fftSize;

            //_finalSource = singleBlockNotificationStream.ToWaveSource();
            _finalSource = NotificationSource.ToWaveSource();

            //Save Bytes Per Second
            _bytesPerSecond = _finalSource.WaveFormat.BytesPerSecond;

            //Init Raw Buffer
            buffer = new byte[_bytesPerSecond / 2];

            //Size of the buffers used for the drawing etc.
            _actualBufferSize = _bytesPerSecond / 2;

            //Init Left and Right Buffer
            //_left = new List<float>(_actualBufferSize);
            //_right = new List<float>(_actualBufferSize);
            //Determine how big our waveform display is (in pixels), multiply by 2 because we have 2 channels
            _waveFormSize = waveFormBox.Width * 2;
            //The actual storage for waveform display data, gets compressed in the drawing function
            _samples = new float[_waveFormSize * 30];

            //init samples with zeros
            for(int i = 0; i < _waveFormSize; i += 2)
            {
                _samples[i] = 0;
                _samples[i + 1] = 0;
            }

            //Init Spectrum
            for (int i = 0; i < _fftSize/2; i++)
            {
                _freqMap[i] = FrequencyToCoordinate(spectrumDisplay.Width, i * (_finalSource.WaveFormat.SampleRate / _fftSize), 20, 20000);
                _decibelSpectrum[i] = spectrumDisplay.Height;
            }

            //init fft buffers
            for(int i = 0; i < _fftSize; i++)
            {
                oldfftBuffer[i] = 0;
                fftBuffer[i] = 0;
            }

            /*
            //Fill Buffers with Zeros to get to right length at the beginning
            for (int i = 0; i < _actualBufferSize; i++)
            {
                _left.Add(0);
                _right.Add(0);
            }
            */

            //Add Read Event for Data Available
            soundInSource.DataAvailable += (s, e) =>
            {
                int read = 1;
                while (read > 0)
                {
                   read = _finalSource.Read(buffer, 0, buffer.Length);
                }
                    //_writer.Write(buffer, 0, read);
            };

            /*
            //Add Notification for Block Reads
            singleBlockNotificationStream.SingleBlockRead += SingleBlockNotificationStreamOnSingleBlockRead;
            */

            NotificationSource.BlockRead += NotificationSourceBlockRead;

            
            
        }

        private void AnalyzerUIShown(object sender, EventArgs e)
        {
            //Start Sound System
            _soundIn.Start();
        }

        /*
        void SingleBlockNotificationStreamOnSingleBlockRead(object sender, SingleBlockReadEventArgs e)
        {
            //Add Samples to Left and Right while Locking these Buffers
            lock (_lockObj)
            {
                _left.Add(e.Left);
                _right.Add(e.Right);
                fftProvider.Add(e.Left, e.Right);
            }         
        }
        */

        void NotificationSourceBlockRead(object sender, BlockReadEventArgs<float> e)
        {
            //Give data to fft factory and update spectrum related stuff
            fftProvider.Add(e.Data, _fftSize * 2);
            UpdateSpectrumData();
            SmoothSpectrumData();
            //Store data in block to do waveform-related stuff
            _block = e.Data;
            //This is necessary because we can only update UI stuff from the main-thread
            this.BeginInvoke((Action)(() =>
            {
                DrawWaveform(waveFormBox.Width, waveFormBox.Height);
                DrawSpectrum(spectrumDisplay.Width, spectrumDisplay.Height);               
            }));
        }

        void UpdateWaveForm()
        {           
            lock(_lockObj)
            { 
                float[] mono = new float[_block.Length / 2];
                //Mono summing of _block, reducing its size by half
                for(int i = 0; i < _block.Length; i += 2)
                {
                    mono[i/2] = _block[i] + _block[i + 1] / 2.0f;
                }

                //Iterate over _samples to shift its elements to make space for our new incoming samples
                //                       8                               1600
                /*
                for(int i = (mono.Length / _granularity) * 2; i < (_waveFormSize); i++)
                {
                //           i -              8                              i
                    _samples[i - (mono.Length / _granularity) * 2] = _samples[i];
                }

                float min = float.MaxValue;
                float max = float.MinValue;

                //Iterate over block to identify max and min in each resulting pixel
                for (int i = 0; i < mono.Length / _granularity * 2; i+=2)
                {
                    //Iterate within one pixel and produce min and max of this pixel as result
                    for (int j = 0; j < _granularity; j++)
                    {
                        max = Math.Max(_block[i/2 * _granularity + j], max);
                        min = Math.Min(_block[i/2 * _granularity + j], min);
                    }
                    //Append min and max to array
                    //                                      8                   + i       
                    _samples[_samples.Length - (mono.Length / _granularity) * 2 + i] = min;
                    _samples[_samples.Length - (mono.Length / _granularity) * 2 + i + 1] = max;
                    int a = 5;
                } 
                */

                //Iterate over _samples to shift its elements to make space for our new incoming samples               
                for(int i = mono.Length; i < (_samples.Length); i++)
                {
                    _samples[i - mono.Length] = _samples[i];
                }
                //Append new samples to the end
                for(int i = 0; i < mono.Length; i++)
                {
                    _samples[_samples.Length - mono.Length + i] = mono[i];
                }
            }                    
        }
        

        void UpdateSpectrumData()
        {
            lock (_lockObj)
            {
                //Store old fft data in old buffer
                oldfftBuffer = fftBuffer;
                //Get new results
                fftProvider.GetFftData(fftBuffer);
            }           
        }

        void SmoothSpectrumData()
        {
            //this doesnt work for some reason
            //But this is supposed to be a recursive filter that combines old and new data to make spectrum movement smoother
            for(int i = 0; i < fftBuffer.Length; i++)
            {
                fftBuffer[i] = _smooth * oldfftBuffer[i] + (1 - _smooth) * fftBuffer[i];
            }
        }

        void CalcSpectrumDecibels()
        {
            //Get Samples
            //UpdateSpectrumData();

            //Convert fft-data to coordinates for the window
            for (int i = 0; i < _spectrumSize; i++)
            {
                //Calc decibels from amplitude
                float decibels = (float)(20 * Math.Log10(Math.Abs(fftBuffer[i])));

                _decibelSpectrum[i] = decibels;
            }
        }

        float FrequencyToCoordinate(int width, float freq, float minFreq, float maxFreq)
        {
            if (freq < minFreq) return 0;
            else if (freq > maxFreq) return width;
            else
            {
                return (((float)Math.Log10(freq) - (float)Math.Log10(minFreq)) / ((float)Math.Log10(maxFreq) - (float)Math.Log10(minFreq))) * width;
            }
        }

        float DecibelsToCoordinate(int height, float dB, float mindB, float maxdB)
        {
            //Inverted because the way the picturebox works
            float decibelPosition = height;
            //Clamping
            if (dB >= mindB && dB <= maxdB)
            {
                decibelPosition = height - (((dB - mindB) / (maxdB - mindB)) * height);
            }
            else if (dB >= maxdB)
            {
                decibelPosition = 0;
            }

            return decibelPosition;
        }

        void DrawWaveform(int width, int height)
        {
            //Get Samples
            UpdateWaveForm();
            //Calc Window middle point vertically
            int center_y = height / 2;

            /*
            //Calc Samples Per Pixel
            int SamplesPerPixel = drawingSamples.Count() / width;
            */

                Image image = waveFormBox.Image;
            Graphics graphics = Graphics.FromImage(image);
            graphics.Clear(Color.Black);

            /*
            for (int i = 0; i < width; i++)
            {

                //Draw White Vertical Lines according to min and max sample in a pixel
                float max = float.MinValue;
                float min = float.MaxValue;
                for (int j = 0; j < SamplesPerPixel; j++)
                {
                    max = Math.Max(max, drawingSamples.ElementAt(i * SamplesPerPixel + j));
                    min = Math.Min(min, drawingSamples.ElementAt(i * SamplesPerPixel + j));
                    if (max == min) max += 0.001f;
                }
                graphics.FillRectangle(Brushes.White, i, min * height / 2 + center_y, 1, (max - min) * height / 2);          
            }
            */
            /*
            for(int i = 0; i < _samples.Length; i += 2)
            {
                graphics.FillRectangle(Brushes.White, i / 2, _samples[i] * height / 2.0f + center_y, 1, (_samples[i+1] - _samples[i]) * height / 2.0f);
            }
            //graphics.FillRectangle(Brushes.White, 0, 0, waveFormBox.Width, waveFormBox.Height);
            */
            for (int i = 0; i < width; i++)
            {
                int SamplesPerPixel = _samples.Length / width;
                //Draw White Vertical Lines according to min and max sample in a pixel
                float max = float.MinValue;
                float min = float.MaxValue;
                for (int j = 0; j < SamplesPerPixel; j++)
                {
                    max = Math.Max(max, _samples[i * SamplesPerPixel + j]);
                    min = Math.Min(min, _samples[i * SamplesPerPixel + j]);
                    if (max == min) max += 0.001f;
                }
                graphics.FillRectangle(Brushes.White, i, min * height / 2 + center_y, 1, (max - min) * height / 2);
            }
            waveFormBox.Refresh();
        }

        void DrawSpectrum(int width, int height)
        {
            //Init Image
            Image image = spectrumDisplay.Image;

            //Convert Bitmap to Graphics
            Graphics graphics = Graphics.FromImage(image);
            graphics.Clear(Color.Black);

            //Initialize Pens to draw Spectrum and Reference Lines for frequency
            Pen whitePen = new Pen(Color.White, 3);
            Pen transparentWhitePen = new Pen(Color.FromArgb(30, 255, 255, 255), 2);
            
            //Window volume boundaries
            float maxdB = 10;
            float mindB = -100;

            //Window frequency boundaries
            float maxFreq = 20000;
            float minFreq = 20;

            //Convert Amplitudes to decibels
            CalcSpectrumDecibels();

            //Draw Logarithmic Scale reference lines for frequency
            for(int i = 1; i < 4; i++)
            {
                for(int j = 2; j < 11; j++)
                {
                    if(j*Math.Pow(10, i) <= 20000)
                    {
                        graphics.DrawLine(transparentWhitePen, FrequencyToCoordinate(width, (float)(j*Math.Pow(10, i)), minFreq, maxFreq), 0, FrequencyToCoordinate(width, (float)(j * Math.Pow(10, i)), minFreq, maxFreq), height);
                    }
                }
            }        

            //Convert fft-data to coordinates for the window
            for (int i = 0; i < _spectrumSize; i++)
            {
                float frequencyPosition = _freqMap[i];
                float decibelPosition = DecibelsToCoordinate(height, _decibelSpectrum[i], mindB, maxdB);
                _spectrumCurve[i].X = frequencyPosition;
                _spectrumCurve[i].Y = decibelPosition;
            }

            //Draw Spectrum
            graphics.DrawLines(whitePen, _spectrumCurve);
            spectrumDisplay.Refresh();
        }

        //This is not used anymore because we refresh once we read a full block
        private void timer1_Tick_1(object sender, EventArgs e)
        {
            /*
            DrawWaveform(waveFormBox.Width, waveFormBox.Height);
            waveFormBox.Refresh();  
            

            DrawSpectrum(spectrumDisplay.Width, spectrumDisplay.Height);
            spectrumDisplay.Refresh();
            */
        }
        
        private void AnalyzerUI_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(1);
        }
    }
}
