﻿using System.Windows.Forms;

namespace AudioVisuCSCore
{
    partial class AnalyzerUI
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.waveFormBox = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.spectrumDisplay = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.waveFormBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spectrumDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // waveFormBox
            // 
            this.waveFormBox.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.waveFormBox.Location = new System.Drawing.Point(0, 1);
            this.waveFormBox.Name = "waveFormBox";
            this.waveFormBox.Size = new System.Drawing.Size(1600, 220);
            this.waveFormBox.TabIndex = 0;
            this.waveFormBox.TabStop = false;
            this.waveFormBox.WaitOnLoad = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // spectrumDisplay
            // 
            this.spectrumDisplay.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.spectrumDisplay.Location = new System.Drawing.Point(1600, 1);
            this.spectrumDisplay.Name = "spectrumDisplay";
            this.spectrumDisplay.Size = new System.Drawing.Size(1600, 220);
            this.spectrumDisplay.TabIndex = 1;
            this.spectrumDisplay.TabStop = false;
            this.spectrumDisplay.WaitOnLoad = true;
            // 
            // AnalyzerUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(3200, 222);
            this.Controls.Add(this.spectrumDisplay);
            this.Controls.Add(this.waveFormBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AnalyzerUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "AnalyzerUI";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AnalyzerUI_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.waveFormBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spectrumDisplay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox waveFormBox;
        private System.Windows.Forms.Timer timer1;
        private PictureBox spectrumDisplay;
    }
}