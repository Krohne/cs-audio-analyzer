# CS Audio Analyzer

An Audio Analyzer using the CSCore Audio Library. Analyzes the Audio Stream coming through the default Windows playpack device.

#TODOs

The Spectrum display needs to be a bit smoother. Firstly the resolution of 1024 in the FFT is a bit low. Simply using zero-padding on the incoming block to stretch it out might already be enough to make a single graph more smooth.
But also the Spectrum display movements are super hectic. I want to implement a recursive filter so each new graph is a combination of the most recent FFT-results and the previous (also smoothed) spectrum. The code for it is already there but it doesn't work.
If things are going well, I want to add a little options menu that u can show/hide by clicking a button in the corner of the UI.
It would contain options like smoothness of the Spectrum display (refer to "Fall Rate" in Bitwigs Spectrum Analyzer or "Average" in Abletons) and maybe stuff like graph/UI color.